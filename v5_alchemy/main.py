from typing import List
from .database import get_db, database_engine
from fastapi import Depends, FastAPI, HTTPException, Response, status
from sqlalchemy.orm import Session
from . import models, schemas


#Creat the table if they don't exists yet
models.Base.metadata.create_all(bind=database_engine)

app = FastAPI()  # Run the Server

# Fetch all data from User table
@app.get('/users', response_model=List[schemas.User_Response])
def get_users(db: Session = Depends(get_db)):
    all_users = db.query(models.User).all()
    return all_users

# Fetch all data from BlogPost table
@app.get('/BlogPost', response_model=List[schemas.BlogPost_Response])
def get_BlogPosts(db: Session = Depends(get_db)):
    all_BlogPosts = db.query(models.BlogPost).all()
    return all_BlogPosts

# Create a new User row
@app.post('/users', response_model=schemas.User_Response)
def creat_user(user_body:schemas.UserPy, db:Session=Depends(get_db)):
    new_user = models.User(**user_body.dict())
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user

# Update a user

@app.update('/users', response_model=schemas.User_Response)
def update_user(user_body:schemas.UserPy, db:Session=Depends(get_db)):
    update_user = models.User(**user_body.dict())
    db.add(update_user)
    db.commit()
    db.refresh(upd
ate_user)
    return update_user
