from jose import jwt
from v4_auth.utilities.jwt_manager import SERVER_KEY, ALGORITHM


# testing the user creation
def test_create_user(client):
    res = client.post("/users", 
                      json={"email":"test.user@domain.lu", "password":"1234"})
    print(res.json())
    assert res.json().get("email") == "test.user@domain.lu"
    assert res.status_code == 201
    
def test_login_user(create_user, client):
    res = client.post("/auth", data={"username": create_user['email'], 
                    "password": create_user['password'] })
    assert res.status_code == 202
    assert res.json().get("token_type") == "bearer"
    #Verify the token data
    payload =  jwt.decode(res.json().get("access_token"), SERVER_KEY, algorithms=[ALGORITHM])
    id = payload.get("user_id")
    assert id == create_user["id"]
    
    
def test_me(create_user, authorized_client):
    res = authorized_client.get('/users/me/')
    assert res.status_code == 200
    assert res.json().get("id") == create_user["id"]
    assert res.json().get("email") == create_user["email"]
    
    

    
    
    